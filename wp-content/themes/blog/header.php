<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=0.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<?php
		$template_directory_uri = get_template_directory_uri();
		wp_enqueue_style('blog',$template_directory_uri.'/assets/css/blog.css');
		wp_head();
	?>
</head>
<body>
	<div class="header">
		<?php 
			wp_nav_menu( 
				array( 
					'theme_location' => 'max_mega_menu_1' 
				) 
			); 
		?>
	</div>
	<?php
		$thumbnail_url = get_the_post_thumbnail_url();
		$style = '';
		if($thumbnail_url){
			$style = 'style="background-image:url('.$thumbnail_url.')"';
		}
	?>
	<div class="wrapper_banner" <?php echo $style; ?>>
		<div class="banner">
			<h1>
				<?php
					if(is_front_page()){
						echo 'CHIA SẼ KIẾN THỨC LẬP TRÌNH';
					} elseif(is_tag()){
						single_tag_title();
					} elseif(is_category()){
						single_cat_title();
					} else{
						the_title();
					}
				?>
			</h1>
			<div class="meta_blog">
				<ul>
					<li class="date"><?php echo get_the_date('d/m/Y'); ?></li>
					<li class="category">
						<?php 
							$cats = get_the_category(get_the_ID());
							if($cats){
								foreach($cats as $cat){
									echo '<a href="'.get_category_link($cat->term_id).'">'.$cat->name.'</a>';
								}
							}
						?>
					</li> 
				</ul>
			</div>
		</div>
	</div>
