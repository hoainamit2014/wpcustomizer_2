<?php
get_header();
$object = get_queried_object(); 
?>
<div class="container-main">
	<div class="breadcrumb">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
	</div>
	<div class="tags">
		<div class="list___blog">
			<div class="list-blog">
				<?php
					$args = array( 
						'post_type' => 'post' , 
						'orderby' => 'date' , 
						'order' => 'ASC' , 
						'posts_per_page' => 10,
						'cat'=> $object->term_id
					);
					$aPosts = new WP_Query( $args );
					if($aPosts->have_posts()){
						while($aPosts->have_posts()){
							$aPosts->the_post();
							get_template_part('template-parts/content','blog_item');
						}
						wp_reset_postdata();
					}else{
						_e('Không có bài viết nào','blog');
					}
				?>
			</div>
			<div class="con__pagination">
				<?php wp_pagenavi(); ?>
			</div>
		</div>
		<?php  get_template_part('template-parts/content','sidebar'); ?>
	</div>
</div>
<?php get_footer(); ?>