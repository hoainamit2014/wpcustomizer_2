<footer>
	<div class="footer">
		<div class="left">
			© Copyright <?php echo date('Y'); ?> by Trịnh Hoài Nam
		</div>
		<div class="right">
			<a href="https://www.facebook.com/namhoai.trinh.9" target="_blank"><i class="fa fa-facebook-square"></i></a>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>