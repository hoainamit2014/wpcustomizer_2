<?php
	$thumbnail_url = get_template_directory_uri().'/assets/images/default-thumbnail.png';
	$thumbnail = get_the_post_thumbnail_url();
	if($thumbnail){
		$thumbnail_url = $thumbnail;
	}
	$link = get_permalink();
?>
<div class="blog-item">
	<div class="blog-wrap"> 
		<div class="blog-thumbnail" style="background-image:url(<?php echo $thumbnail_url;?>);">
			<span></span>
		</div>
		<div class="blog-content">
			<h3 class="blog-title">
				<a href="<?php echo $link; ?>"><?php the_title(); ?></a>
			</h3>
			<div class="blog-meta">
				<ul class="meta-list">
					<li class="date"><?php echo get_the_date('d/m/Y'); ?></li>
					<li class="category">
						<?php
							$category = get_the_category();
							if($category){
								foreach($category as $cat){
									echo '<span><a href="'.get_category_link($cat->term_id).'">'.$cat->name.'</a></span>';
								}
							}
						?>
					</li>
				</ul>
			</div>
			<div class="blog-description">
				<?php echo get_the_excerpt(); ?>
			</div>
			<div class="blog-read-more">
				<a href="<?php echo $link; ?>">
					<?php _e('Xem thêm','blog'); ?>
				</a>
			</div>
		</div>
	</div>
</div>