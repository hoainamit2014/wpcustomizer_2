<div class="sidebar">
	<div class="widget_sidebar">
		<h2><?php _e('BÀI VIẾT MỚI NHẤT','blog'); ?></h2>
		<div class="list-blog">
			<?php
				$args = array( 
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => 5,
					'orderby' => 'date',
					'order' => 'DESC'
				);
				$aPosts = get_posts($args);
				if($aPosts){
					echo '<ul>';
					foreach($aPosts as $post){
						setup_postdata($post);
						echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
					}
					echo '</ul>';
					wp_reset_postdata();
				}
			?>
		</div>
	</div>
	<div class="widget_sidebar">
		<h2><?php _e('BÀI VIẾT XEM NHIỀU','blog'); ?></h2>
		<div class="list-blog">
			<?php
				$args = array( 
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => 5,
					'orderby' => 'date',
					'order' => 'DESC'
				);
				$aPosts = get_posts($args);
				if($aPosts){
					echo '<ul>';
					foreach($aPosts as $post){
						setup_postdata($post);
						echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
					}
					echo '</ul>';
					wp_reset_postdata();
				}
			?>
		</div>
	</div>
	<div class="widget_sidebar">
		<h2><?php _e('DANH MỤC','blog'); ?></h2>
		<div class="list-blog">
			<ul>
				<?php
					$aCategories = get_categories();
					if($aCategories){
						foreach($aCategories as $cat){
							?>
								<li>
									<a href="<?php echo get_category_link($cat->term_id); ?>">
										<?php echo $cat->name; ?>
									</a>
								</li>
							<?php
						}
					}
				?>
			</ul>
		</div>
	</div>
	<div class="widget_sidebar">
		<h2><?php _e('TAGS','blog'); ?></h2>
		<div class="list-tags">
			<?php
				$aTags = get_terms( 'post_tag', array(
					'hide_empty' => false,
				));
				if($aTags){
					foreach($aTags as $tag){
						?>
							<a href="<?php echo get_category_link($tag->term_id); ?>">
								<?php echo $tag->name; ?>
							</a>
						<?php
					}
				}
			?>
		</div>
	</div>
</div>