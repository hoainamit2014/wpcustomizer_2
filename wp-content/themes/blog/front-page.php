<?php
/*
	Template Name: Front Page Template
*/
get_header();
?>
<section class="section_hp">
	<div class="container-main">
		<h2 class="panel_title">WP CƠ BẢN</h2>
		<div class="list-blog">
			<?php for($i = 0; $i < 5; $i++) : ?>
				<div class="blog-item">
					<div class="blog-wrap">
						<?php
							$thumbnail_default = get_template_directory_uri().'/assets/images/default-thumbnail.png';
						?>
						<div class="blog-thumbnail" style="background-image:url(<?php echo $thumbnail_default;?>);">
							<span></span>
						</div>
						<div class="blog-content">
							<h3 class="blog-title"><a href="<?php echo get_permalink(); ?>">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h3>
							<div class="blog-meta">
								<ul class="meta-list">
									<li class="date"><?php echo get_the_date('d/m/Y'); ?></li>
									<li class="category">
										<span><a href="#">#Security</a></span>
									</li>
								</ul>
							</div>
							<div class="blog-description">
								Hành trình đáng lẽ sẽ dừng chân tại Đức theo kế hoạch mà anh đã bàn với ông Lê Minh Tuân, bố của mình. Nhưng khi đến Malaysia, anh gọi điện về nhà thông báo lịch trình chuyển hướng. Hà sẽ bay đến Pháp...
							</div>
							<div class="blog-read-more">
								<a href="<?php echo get_permalink(); ?>">Xem thêm</a>
							</div>
						</div>
					</div>
				</div>
			<?php endfor;?>
		</div>
		<div class="viewmore"><a href="#viewmore">XEM THÊM</a></div>
	</div>
</section>
<section class="section_hp">
	<div class="container-main">
		<h2 class="panel_title">WP NÂNG CAO</h2>
		<div class="list-blog">
			<?php for($i = 0; $i < 5; $i++) : ?>
				<div class="blog-item">
					<div class="blog-wrap">
						<?php
							$thumbnail_default = get_template_directory_uri().'/assets/images/default-thumbnail.png';
						?>
						<div class="blog-thumbnail" style="background-image:url(<?php echo $thumbnail_default;?>);">
							<span></span>
						</div>
						<div class="blog-content">
							<h3 class="blog-title"><a href="<?php echo get_permalink(); ?>">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h3>
							<div class="blog-meta">
								<ul class="meta-list">
									<li class="date"><?php echo get_the_date('d/m/Y'); ?></li>
									<li class="category">
										<span><a href="#">#Security</a></span>
										<span><a href="#">#Security</a></span>
										<span><a href="#">#Security</a></span>
									</li>
								</ul>
							</div>
							<div class="blog-description">
								Hành trình đáng lẽ sẽ dừng chân tại Đức theo kế hoạch mà anh đã bàn với ông Lê Minh Tuân, bố của mình. Nhưng khi đến Malaysia, anh gọi điện về nhà thông báo lịch trình chuyển hướng. Hà sẽ bay đến Pháp...
							</div>
							<div class="blog-read-more">
								<a href="<?php echo get_permalink(); ?>">Xem thêm</a>
							</div>
						</div>
					</div>
				</div>
			<?php endfor;?>
		</div>
		<div class="viewmore"><a href="#viewmore">XEM THÊM</a></div>
	</div>
</section>
<section class="section_hp">
	<div class="container-main">
		<h2 class="panel_title">BẢO MẬT WORDPRESS</h2>
		<div class="list-blog">
			<?php for($i = 0; $i < 5; $i++) : ?>
				<div class="blog-item">
					<div class="blog-wrap">
						<?php
							$thumbnail_default = get_template_directory_uri().'/assets/images/default-thumbnail.png';
						?>
						<div class="blog-thumbnail" style="background-image:url(<?php echo $thumbnail_default;?>);">
							<span></span>
						</div>
						<div class="blog-content">
							<h3 class="blog-title"><a href="<?php echo get_permalink(); ?>">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h3>
							<div class="blog-meta">
								<ul class="meta-list">
									<li class="date"><?php echo get_the_date('d/m/Y'); ?></li>
									<li class="category">
										<span><a href="#">#Security</a></span>
										<span><a href="#">#Security</a></span>
										<span><a href="#">#Security</a></span>
									</li>
								</ul>
							</div>
							<div class="blog-description">
								Hành trình đáng lẽ sẽ dừng chân tại Đức theo kế hoạch mà anh đã bàn với ông Lê Minh Tuân, bố của mình. Nhưng khi đến Malaysia, anh gọi điện về nhà thông báo lịch trình chuyển hướng. Hà sẽ bay đến Pháp...
							</div>
							<div class="blog-read-more">
								<a href="<?php echo get_permalink(); ?>">Xem thêm</a>
							</div>
						</div>
					</div>
				</div>
			<?php endfor;?>
		</div>
		<div class="viewmore"><a href="#viewmore">XEM THÊM</a></div>
	</div>
</section>
<?php get_footer(); ?>