<?php
get_header();
?>
<div class="container-main">
	<div class="breadcrumb">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
	</div>
	<div class="single">
		<div class="single_blog">
			<div class="content">
				<?php the_content(); ?>
			</div>
			<div class="pagination_group">
				<ul>
					<?php
						$prev = get_previous_post_link();
						if($prev){
							echo '<li class="prev_class">'.$prev.'</li>';
						}

						$next = get_next_post_link();
						if($next){
							echo '<li class="next_class">'.$next.'</li>';
						}
					?>
				</ul>
			</div>
		</div>
		<?php  get_template_part('template-parts/content','sidebar'); ?>
	</div>
</div>
<?php get_footer(); ?>