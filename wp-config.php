<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'namthvlog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysql' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'x)1XH0>t*1{|7!e:W4F#e&>wMa3)j+[Ux!+9^/#Q0#JWCk/{4e!En%B(x#qK`^c[' );
define( 'SECURE_AUTH_KEY',  '5%%%ntd$PD`^2P1VL/jxP9+FTj@~XzYZD#K4q9_{XLvy))6HbGAjAUVMHt;V`<m]' );
define( 'LOGGED_IN_KEY',    'Q/V~)#i-tXmwLOkF9Oqn;ksfzk6b^*&H78g7o=dG&M  :<VcMk0<DSjKi4Vj,t2C' );
define( 'NONCE_KEY',        'Nw;CpmQ#L|zTq!.*j-(ZKiD~:*chIYZ!)8`f(WRhG s[(StDW+&OGZJxm(|Z4}Sx' );
define( 'AUTH_SALT',        'A0zi_sBkHABfkVq `Mlz[aFSO_JXrSi_d|XWGhn%L<~/woVdj0U ~~foCl#GP@fa' );
define( 'SECURE_AUTH_SALT', 'zdETSi4O:iU}3!tW_V=BeSM/8.&}FtC~(ZxElNUS..aVN#%w5(~_1]uW/>mbrO1U' );
define( 'LOGGED_IN_SALT',   'u+8~oyg{pVggi?ix.E8K[~OGMhLnJ69l~b:3+8i@!|F^KD<IF/:0TLUAfAm3-c$@' );
define( 'NONCE_SALT',       'e[6p^5}G^Wr/W/B3JyR0VVHMP,iN1$3<q(B`98bjga|F885i{Lw{R-0u2Ki~FFq^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'namth_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
